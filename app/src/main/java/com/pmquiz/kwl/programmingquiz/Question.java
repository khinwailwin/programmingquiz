package com.pmquiz.kwl.programmingquiz;

import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Parcelable {
    private String question_Id;
    private String question;
    private String choiceA;
    private String choiceB;
    private String choiceC;
    private String choiceD;
    private String answer;
    private String qtype;

    public String getQuestion_Id() {
        return question_Id;
    }

    public void setQuestion_Id(String question_Id) {
        this.question_Id = question_Id;
    }

    public String getQ_Type() {
        return qtype;
    }

    public void setQ_Type(String qtype) {
        qtype = qtype;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getChoiceA() {
        return choiceA;
    }

    public void setChoiceA(String choiceA) {
        this.choiceA = choiceA;
    }

    public String getChoiceB() {
        return choiceB;
    }

    public void setChoiceB(String choiceB) {
        this.choiceB = choiceB;
    }

    public String getChoiceC() {
        return choiceC;
    }

    public void setChoiceC(String choiceC) {
        this.choiceC = choiceC;
    }

    public String getChoiceD() {
        return choiceD;
    }

    public void setChoiceD(String choiceD) {
        this.choiceD = choiceD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public static Creator<Question> getCREATOR() {
        return CREATOR;
    }

    public Question(){

    }
    public Question(String question_Id,String question, String choiceA, String choiceB, String choiceC, String choiceD, String answer,String qtype) {

        this.question_Id=question_Id;
        this.question = question;
        this.choiceA = choiceA;
        this.choiceB = choiceB;
        this.choiceC = choiceC;
        this.choiceD=choiceD;
        this.answer = answer;
        this.qtype=qtype;
    }
    public Question(Parcel in) {
        question = in.readString();
        choiceA = in.readString();
        choiceB = in.readString();
        choiceC = in.readString();
        choiceD = in.readString();
        answer=in.readString();
        qtype=in.readString();
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(choiceA);
        dest.writeString(choiceB);
        dest.writeString(choiceC);
        dest.writeString(choiceD);
        dest.writeString(answer);
        dest.writeString(qtype);
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
