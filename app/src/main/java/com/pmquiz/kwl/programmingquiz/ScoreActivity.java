package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class ScoreActivity extends AppCompatActivity {
    private TextView beginner_score,inter_score,advanced_score;
    private CardView beginnerCard,interCard,advacedCard;


    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";

    private TextView title;

    //get score point from sharedpreference using key
    public static final String beginnerScore="beginner";
    public static final String interScore="intermediate";
    public static final String advancedScore="advanced";

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        //sharedprefernce for only this app Context.MODE_PRIVATE
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);

        //action bar title text center using xml
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_txt_center);



        //set title text
        title=(AppCompatTextView) findViewById(R.id.tb_title);
        title.setText("SCORES");

        beginner_score=(TextView) findViewById(R.id.beginner_score);
        inter_score=(TextView) findViewById(R.id.inter_score);
        advanced_score=(TextView) findViewById(R.id.advanced_score);

        beginnerCard=(CardView) findViewById(R.id.beginnerCard);
        interCard   =(CardView) findViewById(R.id.interCard);
        advacedCard =(CardView) findViewById(R.id.advancedCard);

        beginnerCard.setCardBackgroundColor(Color.parseColor("#267B88"));
        interCard.setCardBackgroundColor(Color.parseColor("#F8647F"));
        advacedCard.setCardBackgroundColor(Color.parseColor("#21D2BB"));



        if(mSharedPreferences.contains(beginnerScore)){
                beginner_score.setText("Beginner               "+mSharedPreferences.getString(beginnerScore,"")+" out of 20");
        }
        if(mSharedPreferences.contains(interScore)){
            inter_score.setText("Intermediate               "+mSharedPreferences.getString(interScore,"")+" out of 20");
        }
        if(mSharedPreferences.contains(advancedScore)){
            advanced_score.setText("Advanced               "+mSharedPreferences.getString(advancedScore,"")+" out of 20");
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

}
