package com.pmquiz.kwl.programmingquiz;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LevelFragment extends Fragment {

    private final static String MIPMAP = "mipmap";

    private RecyclerView mRecycleView;
    private Activity mActivity;
    private CardView layout_card;

    public LevelFragment() {

    }

    public static LevelFragment newInstance() {
        LevelFragment fragment = new LevelFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.level_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
        mRecycleView = (RecyclerView) view.findViewById(R.id.level_fragment);
        setLevelView();
    }

    private void setLevelView() {
        List<Level> levelList= new ArrayList<>();
        List<String> levelName =new ArrayList<String>();
        levelName.add("Beginner");
        levelName.add("Intermediate");
        levelName.add("Advanced");
        levelName.add("Scores");
        for(int j=0;j<levelName.size();j++){
            Level level =new Level();
            level.setLevelname(levelName.get(j));
            levelList.add(level);
        }
        loadData(levelList);
        LevelAdapterView levelAdapterView = new LevelAdapterView(levelList, getContext());
        setOnclickListener(levelAdapterView, levelList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        mRecycleView.setLayoutManager(gridLayoutManager);
        mRecycleView.setAdapter(levelAdapterView);
    }

    private void loadData(List<Level> levelList) {
        Resources resources = mActivity.getResources();
        String packageName = mActivity.getPackageName();
        for(Level level:levelList) {
            String iconName = level.getLevelname().toLowerCase();
            int icon = getResourceByName(iconName,"drawable", resources, packageName);
            level.setIcon_id(icon);
        }
    }

    private int getResourceByName(String name, String identifier, Resources resources, String packageName) {
        return resources.getIdentifier(name, identifier, packageName);
    }

    private void setOnclickListener(final LevelAdapterView levelAdapterView, final List<Level> levelList) {
        levelAdapterView.setOnItemClickListener(new LevelAdapterView.OnItemClickListener() {

            public void onClick(View view, int position) {

                Intent i =new Intent(mActivity, AcitivityQuiz.class);
                if(position==0){
                    i.putExtra("level","beginner");
                }else if(position==1){
                    i.putExtra("level","intermediate");
                }else if(position==2){
                    i.putExtra("level","advanced");
                }else {
                    i=new Intent(mActivity,ScoreActivity.class);
                    i.putExtra("level","scores");
                }
                startActivity(i);

            }
        });
    }
}
