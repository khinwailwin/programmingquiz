package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

public class PracticeQuestionActivity extends AppCompatActivity {

    List<Question> questionList;
    int score = 0;
    int quid = 0;
    Question currentQuestion;
    private Vibrator emptyChoice;

    TextView txtQuestion;
    RadioButton rda,rdb,rdc,rdd;
    private CardView questionCard;
    private TextView title;
    Button btnNext;
    String quiz_level;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_question);

        Bundle level= getIntent().getExtras();
        quiz_level=level.getString("level");
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);



        //action bar title text center using xml
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_txt_center);



        //set title text
        title=(AppCompatTextView) findViewById(R.id.tb_title);
        title.setText(quiz_level.toUpperCase());

        QuestionData getQuestion = new QuestionData(getApplicationContext());
        questionList=getQuestion.getAllQuestionswithQtype(level.getString("level"));
        currentQuestion = questionList.get(quid);

        txtQuestion = (TextView)findViewById(R.id.question);

        questionCard=(CardView) findViewById(R.id.questionCard);
        if(quiz_level.toUpperCase().equals("BEGINNER")){
            questionCard.setCardBackgroundColor(Color.parseColor("#267B88"));
        }else if(quiz_level.toUpperCase().equals("INTERMEDIATE")){
            questionCard.setCardBackgroundColor(Color.parseColor("#F8647F"));
        }else{
            questionCard.setCardBackgroundColor(Color.parseColor("#21D2BB"));
        }


        emptyChoice =(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        btnNext = (Button)findViewById(R.id.next);
        setQuestionView();
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                nextQuestion(view);
            }
        });
    }

    private void setQuestionView(){
        txtQuestion.setText((quid+1)+". "+currentQuestion.getQuestion());
        RadioGroup rbg = (RadioGroup)findViewById(R.id.questionRadioGroup);

       if(!currentQuestion.getChoiceA().equals("")){
           rda=new RadioButton(this);
           rda.setText(currentQuestion.getChoiceA());
           rbg.addView(rda);
       }
        if(!currentQuestion.getChoiceA().equals("")){
            rdb=new RadioButton(this);
            rdb.setText(currentQuestion.getChoiceB());
            rbg.addView(rdb);
        }


        if(!currentQuestion.getChoiceC().equals(" ")){
            rdc=new RadioButton(this);
            rdc.setText(currentQuestion.getChoiceC());
            rbg.addView(rdc);
        }
        if(!currentQuestion.getChoiceD().equals(" ")){
            rdd=new RadioButton(this);
            rdd.setText(currentQuestion.getChoiceD());
            rbg.addView(rdd);
        }
        quid++;
    }

    public void nextQuestion(View view){
        RadioGroup rbg = (RadioGroup)findViewById(R.id.questionRadioGroup);
        Log.e("checing radio button ",rbg.getCheckedRadioButtonId()+"");
        if(rbg.getCheckedRadioButtonId()!=-1){
            RadioButton answer = (RadioButton)findViewById(rbg.getCheckedRadioButtonId());

            if(currentQuestion.getAnswer().equals(answer.getText())){
                score++;
                Log.d("Score", "Your score: "+score);
            }
        }else{
                emptyChoice.vibrate(300);
            Toast.makeText(getApplicationContext(),"Choose your answer!",Toast.LENGTH_LONG).show();
                quid--;
        }

        rbg.clearCheck();

        if(quid<questionList.size()){
            rbg.removeAllViews();
            currentQuestion = questionList.get(quid);
            setQuestionView();
        }else{
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString("score",score+"");
            editor.apply();

            Intent intent = new Intent(PracticeQuestionActivity.this, Resultshow.class);
            Bundle b = new Bundle();
            b.putString("level",quiz_level.toUpperCase());
            intent.putExtras(b);

            startActivity(intent);
            finish();
        }


    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
