package com.pmquiz.kwl.programmingquiz;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LevelQuestions {
    @SerializedName("questions")
    private List<Question> questionList;

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }
}
