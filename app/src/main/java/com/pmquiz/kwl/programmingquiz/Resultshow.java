package com.pmquiz.kwl.programmingquiz;

import android.Manifest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;




import java.io.InputStream;

public class Resultshow extends AppCompatActivity {
    private TextView score_result,tb_title;
    private Button home_btn,score_board_btn,save_image_btn;

    private SharedPreferences mSharedPreferences;

    public static final String myShared= "javaMCQ";

    public AssetManager loadImage;

    String level;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultshow);
        Bundle score= getIntent().getExtras();//score.getString("score")
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_txt_center);

        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        loadImage=getAssets();
        tb_title=(AppCompatTextView)findViewById(R.id.tb_title);
        tb_title.setText(score.getString("level"));
        score_result=(TextView) findViewById(R.id.score_result);
        home_btn=(Button) findViewById(R.id.home_btn);
        score_board_btn=(Button) findViewById(R.id.score_board_btn);
        save_image_btn=(Button) findViewById(R.id.save_image_btn);


        level=score.getString("level");
        String shared_score=mSharedPreferences.getString("score","");
        score_result.setText(mSharedPreferences.getString("score",""));
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString(score.getString("level","").toLowerCase(),shared_score);
        editor.apply();
        home_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i=new Intent(Resultshow.this,HomePage.class);
                startActivity(i);
            }
        });
        score_board_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i=new Intent(Resultshow.this,ScoreActivity.class);
                startActivity(i);
            }
        });

        save_image_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                InputStream path;
                Bitmap certi_bitmap=null;
                Log.e("level check ing ", level);
                try{
                    if(level.equals("BEGINNER")){
                        path=loadImage.open("begin_certificate.png");
                    }else if(level.equals("INTERMEDIATE")){
                        path=loadImage.open("inter_certificate.png");
                    }else{
                        path=loadImage.open("advanced_certificate.png");
                    }

                    final int IMAGE_MAX_SIZE = 1200000; // 1.2MP

                    // Decode image size
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(path, null, options);
                    path.close();



                    int scale = 1;
                    while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) >
                            IMAGE_MAX_SIZE) {
                        scale++;
                    }

                    Bitmap resultBitmap = null;
                    if(level.equals("BEGINNER")){
                        path=loadImage.open("begin_certificate.png");
                    }else if(level.equals("INTERMEDIATE")){
                        path=loadImage.open("inter_certificate.png");
                    }else{
                        path=loadImage.open("advanced_certificate.png");
                    }

                    if (scale > 1) {
                        scale--;

                        options = new BitmapFactory.Options();
                        options.inSampleSize = scale;
                        resultBitmap = BitmapFactory.decodeStream(path, null, options);

                        // resize to desired dimensions
                        int height = resultBitmap.getHeight();
                        int width = resultBitmap.getWidth();


                        double y = Math.sqrt(IMAGE_MAX_SIZE
                                / (((double) width) / height));
                        double x = (y / height) * width;

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(resultBitmap, (int) x,
                                (int) y, true);
                        resultBitmap.recycle();
                        resultBitmap = scaledBitmap;

                        System.gc();
                    } else {
                        resultBitmap = BitmapFactory.decodeStream(path);
                    }
                    path.close();

                certi_bitmap=resultBitmap;
                }catch (Exception e){
                    e.printStackTrace();
                }
                Date sign = Calendar.getInstance().getTime();

                SimpleDateFormat signDate = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = signDate.format(sign);

                Bitmap foreground= createBitmapImage(mSharedPreferences.getString("name",""),20.f);
                Bitmap date=createBitmapImage(formattedDate,10.f);
                combineImages(certi_bitmap,foreground,date);
            }
        });

    }


    public void combineImages(Bitmap background, Bitmap foreground,Bitmap date) {

        int width = 0, height = 0;
        Bitmap cs;



        cs = Bitmap.createBitmap(500, 400, Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(cs);
        background = Bitmap.createScaledBitmap(background, 500, 400, true);
        comboImage.drawBitmap(background, 0, 0, null);
        comboImage.drawBitmap(foreground, 10,30, null);
        comboImage.drawBitmap(date,0,160,null);
        saveToGallery(cs);
    }
public Bitmap createBitmapImage(String sr,float fontsize){
    int width = 500;
    int height = 400;
    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);

    Paint paint = new Paint();
    paint.setColor(Color.TRANSPARENT);
    paint.setStyle(Paint.Style.FILL);
    canvas.drawPaint(paint);

    Typeface tf =Typeface.createFromAsset(getAssets(),"Arial Black.ttf");
    paint.setColor(Color.BLACK);
    paint.setAntiAlias(true);
    paint.setTextSize(fontsize);
    paint.setTypeface(tf);
    if(fontsize>10){
        paint.setTextAlign(Paint.Align.CENTER);
    }else{
        paint.setTextAlign(Paint.Align.RIGHT);
    }

    canvas.drawText(sr, (width / 2.f) , (height / 2.f), paint);
    //saveToGallery(bitmap);
    return bitmap;
}
public void saveToGallery(Bitmap b){

     if(isWriteStoragePermissionGranted()){
         MediaStore.Images.Media.insertImage(getContentResolver(), b, "score" , "socre");
         Toast.makeText(getApplicationContext(),"Successfully save your Certificate Image",Toast.LENGTH_LONG);
     }else{
         Log.e("failed","save");
     }


}
    public  boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        }
        else {

            return true;
        }
    }

    public  boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        }
        else {

            return true;
        }
    }
    @Override
    public void onBackPressed() {
        Intent i =new Intent(Resultshow.this,HomePage.class);
        startActivity(i);
        super.onBackPressed();
    }

}
