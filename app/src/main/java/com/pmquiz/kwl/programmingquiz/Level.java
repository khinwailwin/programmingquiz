package com.pmquiz.kwl.programmingquiz;

public class Level {
    private String levelname;
    private String id;
    private String levelIcon;

    private int icon_id;
    private int underline_color_id;

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevelIcon() {
        return levelIcon;
    }

    public void setLevelIcon(String levelIcon) {
        this.levelIcon = levelIcon;
    }

    public int getIcon_id() {
        return icon_id;
    }

    public void setIcon_id(int icon_id) {
        this.icon_id = icon_id;
    }

    public int getUnderline_color_id() {
        return underline_color_id;
    }

    public void setUnderline_color_id(int underline_color_id) {
        this.underline_color_id = underline_color_id;
    }

    public Level() {}
    public Level(String subjectId, String levelName) {
        this.levelname = levelName;
        this.id = subjectId;
    }
}
