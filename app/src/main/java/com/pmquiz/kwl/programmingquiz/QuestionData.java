package com.pmquiz.kwl.programmingquiz;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.pmquiz.kwl.programmingquiz.LevelQuestions;
import com.pmquiz.kwl.programmingquiz.Question;

import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;


public class QuestionData extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "JavaMCQ.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db;
    public static final String TABLE_NAME = "quiz_questions";
    public static final String QUESTION = "question";
    public static final String OPTION1 = "choiceA";
    public static final String OPTION2 = "choiceB";
    public static final String OPTION3 = "choiceC";
    public static final String OPTION4 = "choiceD";
    public static final String ANSWER = "answer";
    public static final String Q_Type = "qtype";
    public static final String Q_ID="question_Id";
    public QuestionData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                Q_ID + " TEXT PRIMARY KEY, " +
                QUESTION + " TEXT NOT NULL, " +
                OPTION1 + " TEXT NOT NULL, " +
                OPTION2 + " TEXT NOT NULL, " +
                OPTION3 + " TEXT, " +
                OPTION4+" TEXT, " +
                Q_Type+" TEXT NOT NULL, "+
                ANSWER + " TEXT NOT NULL);"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        Log.e("test","upgrade");
        onCreate(db);
    }

    public void fillQuestionsTable(Context context) {
        List<Question> questionList= loadJsonQuestion(context);
        for(int i=0;i<questionList.size();i++){

           addQuestion(questionList.get(i));
        }

    }
    public  List<Question> loadJsonQuestion(Context context){
        List<Question> qlist=new ArrayList<Question>();
        LevelQuestions levelQuestions=new LevelQuestions();
        try {
            Gson gson = new Gson();

            Reader reader = new InputStreamReader(context.getAssets().open("questions.json"));
            levelQuestions = gson.fromJson(reader, LevelQuestions.class);
            Log.e("Hello","try"+reader);
        }
        catch (Exception e) {
            Log.e("Hello","catch "+e);
        }
        Log.e("Hello","questions"+levelQuestions.getQuestionList().get(0));
        qlist=levelQuestions.getQuestionList();

        return qlist;

    }
    private void addQuestion(Question question) {
        db=this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(Q_ID,question.getQuestion_Id());
        cv.put(QUESTION, question.getQuestion());
        cv.put(OPTION1, question.getChoiceA());
        cv.put(OPTION2, question.getChoiceB());
        if(!question.getChoiceC().equals(null)){
            cv.put(OPTION3, question.getChoiceC());
        }else{
            cv.put(OPTION3, "");
        }
        if(!question.getChoiceD().equals(null)){
            cv.put(OPTION4, question.getChoiceD());
        }else{
            cv.put(OPTION4, "");
        }
        cv.put(Q_Type,question.getQ_Type());
        cv.put(ANSWER, question.getAnswer());

        db.insert(TABLE_NAME, null, cv);

    }

    public ArrayList<Question> getAllQuestionswithQtype(String q_Type) {
        ArrayList<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME+" WHERE TRIM(qtype) = '"+q_Type.trim()+"'", null);

        if (c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setQuestion(c.getString(c.getColumnIndex(QUESTION)));
                question.setChoiceA(c.getString(c.getColumnIndex(OPTION1)));
                question.setChoiceB(c.getString(c.getColumnIndex(OPTION2)));
                question.setChoiceC(c.getString(c.getColumnIndex(OPTION3)));
                question.setChoiceD(c.getString(c.getColumnIndex(OPTION4)));
                question.setAnswer(c.getString(c.getColumnIndex(ANSWER)));
                question.setQ_Type(c.getString(c.getColumnIndex(Q_Type)));
                questionList.add(question);

            } while (c.moveToNext());
        }
        Log.e("db check", c.moveToFirst()+"");
        c.close();
        db.close();
        return questionList;
    }
}
