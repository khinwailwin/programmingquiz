package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Vibrator;
import android.provider.CalendarContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {


   private Vibrator emptyName;
    EditText userName;
    Button startBtn;
    String user_name;
    private ImageView logoImage;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    public static final String myShared_userName = "name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Custom action bar for title center and bar color
        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#395373")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.main_action_bar);

        //sharedprefernce for only this app Context.MODE_PRIVATE
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        userName = (EditText) findViewById(R.id.urName);
        startBtn = (Button) findViewById(R.id.startBtn);
        logoImage= (ImageView) findViewById(R.id.logoId);

        //Empty name notify with vibrate and no go to quiz category
        emptyName =(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);



        if(mSharedPreferences.contains(myShared_userName)){
            startActivity(new Intent(getApplicationContext(), HomePage.class));
            finish();
        }
        startBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(validateName()){
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putString(myShared_userName,userName.getText().toString());
                    editor.apply();
                    Intent i = new Intent(MainActivity.this, HomePage.class);
                    startActivity(i);
                }else{
                    emptyName.vibrate(300);
                }

            }
        });

    }

    public boolean validateName(){
        user_name= userName.getText().toString();
        return !(user_name.isEmpty());
    }
}
