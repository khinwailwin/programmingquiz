package com.pmquiz.kwl.programmingquiz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AcitivityQuiz extends AppCompatActivity {

    private Button no_of_questions,marks,letStart;
    String quiz_level;
    private TextView title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_main);
        no_of_questions=(Button) findViewById(R.id.no_of_questions);
        marks=(Button) findViewById(R.id.marks);
        letStart=(Button) findViewById(R.id.letStart);

        //action bar title text center using xml
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_txt_center);




        //get level from level button click
        Bundle level= getIntent().getExtras();
        quiz_level=level.getString("level");

        //set title text
        title=(AppCompatTextView) findViewById(R.id.tb_title);
        title.setText(quiz_level.toUpperCase());

        if(savedInstanceState==null){
            if(quiz_level.toUpperCase().equals("BEGINNER")){
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#267B88")));
                no_of_questions.setBackground(getResources().getDrawable(R.drawable.quiz_button));
                marks.setBackground(getResources().getDrawable(R.drawable.quiz_button));
                letStart.setBackground(getResources().getDrawable(R.drawable.quiz_button));
            }else if(quiz_level.toUpperCase().equals("INTERMEDIATE")){
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F8647F")));
                no_of_questions.setBackground(getResources().getDrawable(R.drawable.inter_quiz_button));
                marks.setBackground(getResources().getDrawable(R.drawable.inter_quiz_button));
                letStart.setBackground(getResources().getDrawable(R.drawable.inter_quiz_button));
            }else{
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#21D2BB")));
                no_of_questions.setBackground(getResources().getDrawable(R.drawable.advanced_quiz_button));
                marks.setBackground(getResources().getDrawable(R.drawable.advanced_quiz_button));
                letStart.setBackground(getResources().getDrawable(R.drawable.advanced_quiz_button));
            }

        }

        letStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(AcitivityQuiz.this, PracticeQuestionActivity.class);
                if(quiz_level.toLowerCase().equals("beginner")){
                    i.putExtra("level","beginner");
                }else if(quiz_level.toLowerCase().equals("intermediate")){
                    i.putExtra("level","intermediate");
                }else if(quiz_level.toLowerCase().equals("advanced")){
                    i.putExtra("level","advanced");
                }
                startActivity(i);
            }
        });
    }
}
