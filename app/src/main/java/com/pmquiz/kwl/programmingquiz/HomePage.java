package com.pmquiz.kwl.programmingquiz;


import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.KeyEvent;
import android.widget.Toast;

public class HomePage extends AppCompatActivity {

     Button beginner, intermediate, advanced, score;

     private TextView title;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //action bar title text center using xml
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_txt_center);



        //set title text
        title=(AppCompatTextView) findViewById(R.id.tb_title);
        title.setText("JMC");
        QuestionData questionData=new QuestionData(this);
        questionData.fillQuestionsTable(this);
        if(savedInstanceState==null){
            getSupportActionBar().setTitle("JMC");

            levelFragmentGrid();
        }

    }

    public void levelFragmentGrid(){
        FragmentManager level_fragment= getSupportFragmentManager();
        level_fragment.beginTransaction().replace(R.id.fragment_container, LevelFragment.newInstance()).commit();

    }
    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }

}
